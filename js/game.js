class Game{
    constructor(gamesList){
        this.MAX_PRAYER_PER_ROOM = 4;
        this.email = account.user.email;
        this.people = 0;
        this.IDList = [];
        this.playerID = 0;
        this.targetID = 0;
        this.targetOnline = 0;
        this.targetLast = 0;
        this.monitorTime = 0;
        this.room = null;
        this.gamesList = gamesList;

        var game = this;
        $("#welcome-page").slideUp(1000,function(){
            $(".load-mes").html("finding room");
            $(".loading-page").show();
        });

        this.findRoom(function(room){
            if(room===null){
                room = game.createRoom();
                console.log("not find a room! new room :" + room);
            }
            else{
                console.log("find a room!" + room);
            }
            game.room = room;
            game.addPlayer(function(){
                // load scoreBroad
                game.loadScoreBroad();
                var people = String(game.people);
                var scoreMes =  $("#score-broad p").html() + people + "/" + game.MAX_PRAYER_PER_ROOM;
                $("#score-broad p").html(scoreMes);
                // load canvas and chatroom
                chatRoom = new ChatRoom;  
                canvas = new Canvas;     
                chatRoom.send( account.name + " join the room","system");
                game.onlineFlag = 0;
                setInterval(game.online,1000);
                // find target
                game.findTarget(function(targetID){
                    game.targetID = targetID;
                    // listening target change
                    var onlineRef = firebase.database().ref('games/'+ game.room +'/player/' + targetID); 
                    onlineRef.on("child_changed", function(snapshot) {
                        game.targetOnline++;
                    });

                    setInterval(game.monitor,2000);
                    game.start();
                    setTimeout(function(){
                        $(".loading-page").hide();
                        $("#main-page").show("slow",function(){
                            canvas.resize();
                        });
                    },1500);
                });
            });
        });
    }
    findRoom(callback){
        var game = this;
        if(game.gamesList===null){
            firebase.database().ref('gamesList').once('value').then(function(snapshot) {
                var room = null;
                snapshot.forEach(function(child){
                    var data = child.val();
                    if(data.people < game.MAX_PRAYER_PER_ROOM){
                        game.gamesList = child.key;
                        game.people = data.people;
                        room = data.key;
                    }
                    else{
                        console.log("room is full");
                    }
                });
                callback(room);
            });
        }
        else{ // find room with roomcode
            firebase.database().ref('gamesList/' + game.gamesList).once('value').then(function(snapshot) {
                var data = snapshot.val();
                game.people = data.people;
                var room = data.key;
                callback(room);
            });
        }
    };
    createRoom(){
        var newGameKey = firebase.database().ref('games').push({
            host : 1
        }).key;
        this.gamesList = firebase.database().ref('gamesList').push({
            people : 0,
            key : newGameKey
        }).key;
        return newGameKey;
    }
    addPlayer(callback){
        var game = this;
        var playerRef = firebase.database().ref('games/'+ this.room +'/player'); 
        var ID = 0;
        playerRef.once('value').then(function (snapshot) {
            snapshot.forEach(function(child){
                ID = child.key;
            });
            playerRef.child(Number(ID)+1).update({
                email : game.email,
                score : 0,
                online : 0
            });
            callback();
        });
        firebase.database().ref('gamesList/'+ game.gamesList).update({
            people : game.people + 1
        });
        game.people++;
    }
    loadScoreBroad(){
        // load previous message
        var game = this;
        var playerRef = firebase.database().ref('games/'+ this.room +'/player');
        var firstCount = 0;
        var secondCount = 0; 

        playerRef.once('value').then(function (snapshot) {
            game.IDList = [];
            snapshot.forEach(function(child){
                var player = child.val(); 
                console.log(player);
                if(player.email===account.email){
                    game.playerID = child.key;
                }
                var name = player.email.split("@")[0];
                var score = "<p class = \"ID" + child.key + " \">" + name + " : " + player.score + "</p>";
                firstCount += 1;
                $("#player-list").html($("#player-list").html()+score);
                
                // console.log(player.ID);
                game.IDList.push(child.key);
            });
            // add linstener to new data
            playerRef.on("child_changed", function(child){
                var player = child.val();
                var name = player.email.split("@")[0];
                var query = "#player-list .ID" + child.key;
                $(query).html( name + " : " + player.score);
            });
            playerRef.on("child_added", function(child){
                secondCount+=1;
                if(secondCount>firstCount){
                    var player = child.val();
                    var name = player.email.split("@")[0];
                    var newPlayer = "<p class = \"ID" + child.key + "\">" + name + " : " + player.score + "</p>";
                    $("#player-list").html($("#player-list").html() + newPlayer);

                    game.people++;
                    var people = String(game.people);
                    var scoreMes = people + "/" + game.MAX_PRAYER_PER_ROOM;
                    $("#score-broad>p").html("Score broad " + scoreMes);

                    game.IDList.push = child.key;

                    // updatetarget
                    game.updateTarget();
                }
            });
            // == handling player delete
            playerRef.on("child_removed", function(child){
                // remove leaderbroad
                console.log("player removed");
                var player = child.val();
                var name = player.email.split("@")[0];
                var query = "#player-list .ID" + child.key;
                $(query).remove();

                firebase.database().ref('gamesList/'+ game.gamesList).once('value').then(function(snapshot) {
                    // console.log(snapshot.val().people);
                    game.people = snapshot.val().people;
                    var people = String(game.people);
                    var scoreMes = people + "/" + game.MAX_PRAYER_PER_ROOM;
                    $("#score-broad>p").html("Score broad " + scoreMes);
                });
            });

        });
    }
    online(){
        game.onlineFlag+=1;
        var playerRef = firebase.database().ref('games/'+ game.room +'/player');
        playerRef.child(game.playerID).update({
            online : game.onlineFlag
        });
    }
    monitor(){
        // if target online
        game.monitorTime++;
        if(game.targetLast!==game.targetOnline){
            // nothing
        }
        // else 
        else{
            // if target is host
                // find new host
            // system : ??? leave the room
            // if people = 2 and the first call
            var targetRef = firebase.database().ref('games/'+ game.room +'/player/' + game.targetID);
            targetRef.once('value').then(function(snapshot) {
                var email = snapshot.val().email;
                if(game.people===2 && game.monitorTime===1){
                    // clear the chatroom
                    firebase.database().ref('games/'+ game.room +'/chatRoom').remove();
                    canvas.clear();
                    $("#chat-mes").html("<span class=\"system\">chat here~</span>");
                }
                else{
                    chatRoom.send( email.split("@")[0] + " leave the room","system");
                }
                // delete player
                targetRef.remove();
                game.updateTarget();
                // change gamelist
                firebase.database().ref('gamesList/' + game.gamesList).update({
                    people : game.people - 1
                });
            });
        }
        game.targetLast = game.targetOnline;
    }
    start(){
        // listening host change
        console.log("child_change");
        firebase.database().ref('games/'+ game.room +'/host').on('child_changed',function(child){
            // change leader broad
            // if host == ID
                // enable painter
                // load words
                // randomly choose one word
                // set to database
                // system : ??? is drawing
                // counting
                // listening times up or everone is correct
                    // find next host
                    // change host
            // else
                // disable painter
        });
    }
    checkAnswer(callback){
        // get database
    }
    findTarget(callback){
        var playerRef = firebase.database().ref('games/'+ this.room +'/player');
        playerRef.once('value').then(function (snapshot) {
            game.IDList = [];
            snapshot.forEach(function(child){   
                game.IDList.push(child.key);
            });
            var target = game.IDList[0];
            var min = 100000;
            for(var i=0; i<game.IDList.length; i++){
                if(game.IDList[i]-game.playerID > 0){
                    if(game.IDList[i]<min){
                        min = game.IDList[i];
                    }
                }
            }
            if(min!==100000){
                target = min;
            }
            callback(target);
        });
    }
    updateTarget(){
        game.findTarget(targetID=>{
            var onlineRef = firebase.database().ref('games/'+ game.room +'/player/' + game.targetID); 
            // cancel old monitor
            onlineRef.off("child_changed");
            // listening new target
            onlineRef = firebase.database().ref('games/'+ game.room +'/player/' + targetID); 
            onlineRef.on("child_changed", function(snapshot) {
                game.targetOnline++;
            });
            // update new target
            game.targetID = targetID;
        });        
    }
}