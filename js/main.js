var account;
var canvas;
var chatRoom;
var game;

$(function(){
    account = new Account;
    $("#btnLogin").click(function(){
        account.login("email");
    });
    $("#btngoogle").click(function(){
        account.login("google");
    });
    $("#btnSignUp").click(function(){
        account.signup();
    });
    $(".nav .btn").click(function(){
        alert("your room code is\n" + game.gamesList);
    });
    $("#btnPlayWithFriend").click(function(){
        var roomCode = $("#roomCode").val();
        findRoomWithCode(roomCode, function(isFound){
            if(isFound===1){
                game = new Game(roomCode);
            }
            else{
                alert("can't find room");
            }
        });
    });
    $("#painter .title").click(function(){
        window.location.href = ".";
    });
    // responsive
    resize();
    $( window ).resize(function(){
        resize();
    });
    // ========================================
    $("#btnPlay").click(function(){
        game = new Game(null);
    });
    // event for canvas
});

// responsive function
function resize(){
    var height = $(window).height();
    $("#chat-mes").css("height",height-190);
}

function findRoomWithCode(roomCode, callback) {
    var isFound = 0; 
    firebase.database().ref('gamesList').once('value').then(function(snapshot) {
        console.log(snapshot.val());
        snapshot.forEach(function(child){
            if(child.key===roomCode){
                isFound = 1;
            }
        });
        callback(isFound);
    });
};

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}