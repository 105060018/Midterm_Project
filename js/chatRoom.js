class ChatRoom{
    constructor(){
        console.log("construct chatroom");
        this.totalMes = [];
        this.chatRef = firebase.database().ref('games/'+ game.room +'/chatRoom');
        this.mesHeight = 0;
        this.h = $("#chat-mes").height();

        var chatRoom = this;
        var firstCount = 0;
        var secondCount = 0; 
        // load previous message
        this.chatRef.once('value').then(function (snapshot) {
            snapshot.forEach(function(child){
                var data = child.val();
                chatRoom.put(data.mes, data.type);
                firstCount += 1;
            });
            // add linstener to new data
            chatRoom.chatRef.on("child_added", function(child){
                secondCount+=1;
                if(secondCount>firstCount){
                    var data = child.val();   
                    chatRoom.put(data.mes, data.type);
                }
            });
        });


        // send new mes
        $("#send-btn").click(function(){
            chatRoom.send($('#send-mes').val(),'player');
        });
        // keydown 'enter'
        document.addEventListener('keydown', function(event) {
            if(event.keyCode===13){
                chatRoom.send($('#send-mes').val(),'player');
            }
        });
    }
    send(mes, type){
        if(type==='player'){
            if(mes!=="" && mes!=="guess or chat here"){
                // if check the answer
                    // system : ??? guess the word
                    // game.add point
                // else
                $('#send-mes').val("");
                var sendMes = account.name + " : " + mes;
                this.chatRef.push({
                    name : account.email,
                    mes : sendMes,
                    type : type
                });
            }
        }
        else if(type === 'system'){
            this.chatRef.push({
                name : account.email,
                mes : mes,
                type : type
            });            
        }
    }
    // put on chat html
    put(mes, type){
        var html = $("#chat-mes").html();
        if(type==="player"){
            $("#chat-mes").html( html +"<span>" + mes + "</span>");
        }
        else if(type==="system"){
            $("#chat-mes").html( html +"<span class=\"system\">" + mes + "</span>");
        }
        // make scroll down to the buttom
        this.mesHeight += this.h;
        $("#chat-mes").scrollTop(this.mesHeight);
    }
};