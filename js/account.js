class Account { 
    constructor() {
        console.log("construct Account");
        this.user = null;
        this.name = "";
        this.email = "";

        firebase.auth().onAuthStateChanged( user => {
            this.user = user;
            if(user){
                var account = this;
                var userName = user.email.split("@");
                this.email = user.email;
                this.name = userName[0];
                $("#welcome-mes").html("Hello " + this.name + "!! <a href=\"#\" id= \"btnLogout\"> signout </a>");
                $( "#btnLogout" ).click(function() {
                    account.logout();
                });
                $("#signin-section").hide(1000);
                $( "#play-section" ).show(1000);
            }
            else{
                $("#welcome-mes").html("Please sign in/register");
                $("#signin-section").show( 1000 );
                $("#play-section").hide( 1000 );
            }
            $(".loading-page").hide(1000);
            $("#welcome-page").slideDown(1000);
            
        });
    }
    logout(){
        firebase.auth().signOut().then(function (result) {
            console.log("logout");
        }).catch(function (error) {
            console.log(error.message);
        });
    }
    login(method){
        if(method==="email"){
            var email = $("#inputEmail").val();
            var password = $("#inputPassword").val();
            firebase.auth().signInWithEmailAndPassword(email, password).then(function(user){
            }).catch(e => create_alert("error",e.message));
        }
        else if(method==="google"){
            var provider = new firebase.auth.GoogleAuthProvider();
            firebase.auth().signInWithPopup(provider).then(function (result) {
                var token = result.credential.accessToken;
                var user = result.user;
            }).catch(e => create_alert("error",e.message));
        }
    }
    signup(){
        var email = $("#inputEmail").val();
        var password = $("#inputPassword").val();
        firebase.auth().createUserWithEmailAndPassword(email, password).catch(e => create_alert("error",e.message));
    }
};