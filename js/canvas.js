class Canvas{
    constructor(){
        this.enable = 1;
        this.strokes = [];
        var canvas = this;
        
        console.log("construct canvas");

    }
    clear(){
        firebase.database().ref('games/'+ game.room +'/painter').set(null);
    }
    draw(){
        var ctx = document.getElementById('canvas').getContext('2d');
        
        canvas.strokes.forEach(path => {
            ctx.beginPath();
            ctx.moveTo(path[0].x,path[0].y);
            path.forEach(point =>{
                ctx.lineTo(point.x,point.y);
            });
            ctx.stroke();
        });
    }
    resize(){
        var path = [];
        $("canvas").attr("height",$("canvas").height());
        $("canvas").attr("width",$("canvas").width());
        // listening mouse move (or for touch)
        $("canvas").mousedown(function(event){
            if(canvas.enable===1){
                path.push({
                    x : event.clientX - $("canvas").offset().left,
                    y : event.clientY - $("canvas").offset().top
                });
                $("canvas").mousemove(function(event){
                    path.push({
                        x : event.clientX - $("canvas").offset().left,
                        y : event.clientY - $("canvas").offset().top
                    });
                });
                $("canvas").mouseup(function () {
                    $("canvas").off("mousemove");
                    firebase.database().ref('games/'+ game.room +'/painter').push(path);
                    path = [];
                });
            }
        });
        firebase.database().ref('games/'+ game.room +'/painter').on('child_added', function(child){
            console.log("???");
            var data = child.val();
            var ctx = document.getElementById('canvas').getContext('2d');
            ctx.beginPath();
            ctx.moveTo(data[0].x,data[0].y);
            data.forEach(point =>{
                ctx.lineTo(point.x,point.y);
            });
            ctx.stroke();
        });
    }
}