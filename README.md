# Midterm project - Draw Something
### by 電資20 105060018 梁景勛

## 簡介
    網站網址1：https://draw-something-15c35.firebaseapp.com/  
    網站網址2：https://105060018.gitlab.io/Midterm_Project/
    
就是一個你畫我猜的遊戲，有chat room的功能，讓玩家可以邊猜邊聊天。
原本是這樣的啦，後來就變成一個聊天室但大家可以一起畫同個圖。

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
* login logout
    * login with email and password
    * login with google account
    * (css animation) change to loading page while running
    * (css animation) loading page logo
    * (RWD) title font-size login-btn can change size due to eindow's size
* play page
    * 找到一個不超過最大人數的房間
    * 如果沒有房間，就自己創建一個
    * 如果大家都離開了，第一個進去空房間的人要把chatroom跟canvas都清空
    * 可以輸入roomCode來找房間加入，如果找不到的話會跳alert
    * (RWD)頁面排版會隨之變動
* chatRoom
    * 能載入先前的對話
    * 當有人離開或加入房間時，都會更新聊天室，與旁邊的上線清單
* painter
    * (RWD)canvas大小也會跟著變動
    * 線上同步畫圖，當有人畫圖時，其他人也可以看到在畫圖


## worklist
### 已完成
* 簡單登入系統（from lab06）
* 簡單post系統（from lab06）
* 登入頁面外觀與動畫
* game
    * 自動新增與尋找 game room,如果人太多會排到新的房間
* chat room
    * 按下enter或send之後把訊息傳上去，並且會自動把頁面捲到最下面

### 必須完成
* 登出（unload設定）移除leaderBroad
* 登入時加入進入房間的mes
* 主遊戲程式，給每一個人一個index值，決定誰要畫
* 從資料庫中選擇一個題目
* 其他人開始接收判斷答案，答對的計分，並在聊天室上顯示
* 時間到時，獲現在在畫的人離開時，在派下一個人
* 第一個進入遊戲間的要把其他都清空
* 通知系統
* -------------
* 基本頁面架構
* 基本外觀與基本動畫
* 小畫家
* 留言區，猜題區
* 輸入特定的chat room code

### 進階功能
* open new window to execute window.closed
* 外觀
    * 開始頁面時標題要滑下來
    * 登入完成的時候也要animate
    * 更換背景圖
* 安全性
    * global 少用
    * account.getName 完成
* Bug
    * 同使用名(solved)
    * 把leaderbroad的索引換成player id(solved)
* 顯示先前
* 加入電子郵件認證與更好的註冊
* music~
* tutorial